
class: center, middle, inverse, big

# Create Your Own HTML Tags with Vue and Custom Elements

## by Joe Erickson

## jerickson.net

---

![Sponsors](img/JSFSponsors.PNG)

---

class: center, middle, big

# Multi-page Appers in the House?

???

- Who here programs solely in Vue Single page applications?
- Who here works on web applications that are normal multipage applications?

---

class: center, middle, big

# Do you feel restricted from using modern component-based development?

???

- What if I told you...
- Creating components in Vue that can be used on any web page.
- Being a SPA is not required and even using only Vue is not required.

---

class: middle

# Topics

1. Web Components Standard
2. Creating a Vue Component
3. Using Components as Web Components, or Custom Elements
4. Cross Communication of Components

---

class: middle

# Web Component Standard

Web Components is a suite of different technologies allowing you to create reusable custom elements — with their functionality encapsulated away from the rest of your code — and utilize them in your web apps.

???

- What is the Web Components standard?
- READ
- These technologies are actually pretty interesting themselves.

---

class: middle

# HTML Templates

Allows you to use template tags and slot tags in your code to define DOM templates.

???

Used in Vue now for its UI.

---

# HTML Templates Example

--

Wrap tag:
``` html
<template>
  <ol>
    <li><slot /></li>
  </ol>
</template>
```

--

``` html
<wrap>Text <em>and</em> HTML</wrap>
```

--

``` html
<ol>
  <li>Text <em>and</em> HTML</li>
</ol>
```

---

![Can I Use stats for HTML Templates](img/caniuse-html-template.png)

---

class: middle

# Shadow DOM

Allows rendering of DOM in a structure that is not the main DOM that the user sees. All elements and styles applied to the shadow DOM do not affect the main DOM.

???

- Used by Vue in a couple of different ways.
- Encapsulation.
- Acts as a DOM within a DOM.

---

![Can I Use stats for Shadow DOM](img/caniuse-shadow-dom.png)

---

class: middle

# Custom Elements

Allows the definition of custom HTML elements with behavior set by you. These are registered with the browser and act as native HTML tags.

???

- It's built into the browser.
- Once the element is defined and registered in the browser, whenever that tag is seen, your code will run.

---

![Can I Use stats for Custom Elements](img/caniuse-custom-elements.png)

---

class: middle, center, big

# Cool! Why not just build a pure JavaScript Custom Element?

???

- These technologies aren't specific to Vue.
- We could program in vanilla JavaScript.
- Why not just do that?
- Because it's ugly.

---

class: middle, center, big, inverse

# It's Ugly!

---

class: middle, center, big

# Let's take a look at a Word Count element

---

# Vanilla Custom Element

``` JavaScript
// Create a class for the element
class WordCount extends HTMLParagraphElement {
  constructor() {
    // Always call super first in constructor
    super();

    // count words in element's parent element
    const wcParent = this.parentNode;

    function countWords(node){
      const text = node.innerText || node.textContent;
      return text.split(/\s+/g).length;
    }

    const count = `Words: ${countWords(wcParent)}`;
```

---

# Vanilla Custom Element Continued...

``` JavaScript
    // Create a shadow root
    const shadow = this.attachShadow({mode: 'open'});

    // Create text node and add word count to it
    const text = document.createElement('span');
    text.textContent = count;

    // Append it to the shadow root
    shadow.appendChild(text);

    // Update count when element content changes
    setInterval(function() {
      const count = `Words: ${countWords(wcParent)}`;
      text.textContent = count;
    }, 200);
  }
}

// Define the new element
customElements.define('word-count', WordCount, { extends: 'p' });
```

???

- With the shadow DOM, we still have to access and use it like we do the regular DOM in vanilla JavaScript.
- It's the same hassles it's always been.
- Imagine a more complex example than this. It wouldn't be pretty.

---

# Example

<article>
<p contenteditable="true">This is a paragraph with words in it for the word count to count.</p><p is="word-count"></p>
</article>

``` HTML
<article>
<p contenteditable="true">
  This is a paragraph with words in it for the word count to count.
</p>
<p is="word-count"></p>
</article>
```

???

- The `<p is=>` is a little weird also.
- Couldn't get it to work with out that. I think that's because we inherited from `<p>`?
- Not really a custom element, is it.

---

class: middle, center, big, inverse

# Is there a better way?

---

# How do we use Vue to make Custom Elements?

--

1. Use the CLI

???

- We're going to use the CLI to create our project, number one.

--

1. Build the components using `@vue/web-component-wrapper`
???

- If you can't use the CLI for some reason, that wrapper is what you want to use.
- It handles all the registration code to the browser for you.

--

1. Include the Vue library and the script on your site
???

- The component will not include the Vue code, so you'll need to include that first.
- You wouldn't want it to, you only need Vue once if you have more than one element.

---

class: center, middle, big, inverse

# Let's build some custom elements!

???

And what should we build? First, a history lesson.

---

# Cast your mind back to 1997

???

Internet was really taking off.

--

- Deep Blue beats Kasparov

???

This was less AI and more brute force calculation, but it was amazing at the time.

--

- Microsoft buys a stake in Apple, saving the company

???

Remember that! The big brother face of Bill Gates behind the timid Steve Jobs!

--

- *Harry Potter and the Philosopher's Stone* is published

--

- **All major browsers have a `<blink>` tag**

???

Truly, the golden age of the Internet. It's gone now, but not forgotten. And `<marquee>` is still around, what's with that?

---

class: middle, center, big

# Let's <my-blink>Blink!</my-blink>

---

class: middle

# Create your project with Vue CLI

``` bash
vue create my-custom-elements
```

???

- It doesn't matter what the project is named.
- No extra options needed.
- You can set up your project just like you always do and use all the tools you usually use.
- The secret is in the build, not in how you set it up.

---

# Create a new component

Blink.vue:
``` html
<template>
  <span ref="blinkyText"><slot /></span>
</template>
```

???

- What is the UI of this component?
- Everything in the `<template>` tags will be put in the place of our tag.
- `<slot />` is used to signify where the children of our blink tag will go.
- Anything that is in between the opening and closing tags of the custom element will be put where `<slot />` is now.

---

# Create a new component

Blink.vue:
``` html
<template>...</template>

<script>
export default {
  name: "blink",
  mounted() {
    setInterval(() => {
      this.$refs.blinkyText.classList.toggle("onoff");
    }, 500);
  }
};
</script>
```

???

- Logic.
- The `name` here is sort of going to be the name of the custom element. I'll get to that when we build it.
- When the component is mounted on the page, we'll kick off an interval.
- That interval will kick off a class toggle on the span element (with reference 'blinkyText') to toggle a class. What is that class?

---

# Create a new component

Blink.vue:
``` html
<template>...</template>

<script>...</script>

<style>
.onoff {
  visibility: hidden;
}
</style>
```

???

- Just a style that will hide and show the tag.
- Because of shadow DOM, these styles are scoped.

---

# Building the component

???

- Vue components with the Vue CLI has a number of build targets.
- By default, it will build the Vue project as an application
- We want to change that.

--

package.json
``` json
"scripts": {
  "serve": "vue-cli-service serve",
* "build": "vue-cli-service build",
  "lint": "vue-cli-service lint"
},
```

???

- The scripts portion of our package.json file has these three scripts defined.
- But if we want custom elements from our component, we're going to need to change this a bit.

---

# Building the component

package.json
``` json
"scripts": {
  "serve": "vue-cli-service serve",
* "build": "vue-cli-service build --target wc",
  "lint": "vue-cli-service lint"
},
```

???

- We are going to add a new build target to the line.
- `wc` is the target to build using the web component standard.
- This will use that wrapper we saw earlier to wrap our component code in the registration code to get our tag defined in the browser.

---

# Building the component

package.json
``` json
"scripts": {
  "serve": "vue-cli-service serve",
* "build": "vue-cli-service build --target wc --name my-blink",
  "lint": "vue-cli-service lint"
},
```

???

- Adding a name is going to set the actual tag used to call out our custom element.
- Could I just name it `blink`? No, not really.
- In the HTML spec, it says that all custom elements must have a `-` in their name, so that they aren't confused with built in elements
- You are also supposed to namespace your tags with a unique name before the dash so as not to conflict with other custom elements on a page.

---

# Building the component

package.json
``` json
"scripts": {
  "serve": "vue-cli-service serve",
* "build": "vue-cli-service build --target wc --name my-blink
    'src/components/Blink.vue'",
  "lint": "vue-cli-service lint"
},
```

???

- Since we only have one component at the moment, we just define which file we want to build.

---

# Building the component

<pre><span style="background-color:#333333"><font color="#999999"> ~/P/v/</font></span><span style="background-color:#333333"><font color="#FFFFFF"><b>vue-blink </b></font></span><span style="background-color:#CE000F"><font color="#333333"> </font></span><span style="background-color:#CE000F"><font color="#FFFFFF"> *… </font></span><font color="#CE000F"> </font><font color="#005FD7">npm</font> <font color="#00AFFF">run</font> <font color="#00AFFF">build</font>

&gt; vue-blink@0.1.0 build /Projects/vue-custom-elements/vue-blink
&gt; vue-cli-service build --target wc --name my-blink &apos;src/components/Blink.vue&apos;


<font color="#1A92AA">⠏</font>  Building for production as web component...

<span style="background-color:#44AA44"><font color="#000000"> DONE </font></span> <font color="#44AA44">Compiled successfully in 2157ms</font>

<font color="#1A92AA">⠋</font>  Building for production as web component...

<span style="background-color:#44AA44"><font color="#000000"> DONE </font></span> <font color="#44AA44">Compiled successfully in 2224ms</font>

  <font color="#4CCCE6"><b>File</b></font>                    <font color="#4CCCE6"><b>Size</b></font>                      <font color="#4CCCE6"><b>Gzipped</b></font>

  <font color="#44AA44">dist/my-blink.min.js</font>    7.40 KiB                  3.17 KiB
  <font color="#44AA44">dist/my-blink.js</font>        25.28 KiB                 7.01 KiB

  <font color="#777777">Images and other types of assets omitted.</font>
</pre>

???

- We can now run `npm run build`
- our component will be built into the `dist` folder.

---

# Building the component

demo.html:
``` html
<meta charset="utf-8">
<title>my-blink demo</title>
<script src="https://unpkg.com/vue"></script>
<script src="./my-blink.js"></script>


<my-blink></my-blink>
```

???

- You also get a nice demo.html file that you can use to try your new custom element out.
- You can see here that you need to include the vue code too
- Then the JavaScript code for your custom element to work.

---

class: center, middle, big

# We have <my-blink>Blinking!!!!!</my-blink>

---

class: center, middle, big, inverse

# What about attributes?

???

- Can we add attributes to the custom element?
- Yes, just like we do in Vue, using props.

---

# Adding attributes to the custom element

``` html
<template>...</template>

<script>
export default {
  name: "blink",
  mounted() {
    setInterval(() => {
      this.$refs.blinkyText.classList.toggle("onoff");
*   }, this.interval);
  },
* props: {
*   interval: {
*     type: Number,
*     default: 500
*   }
  }
};
</script>
```

---

# Using the attributes

```html
<my-blink-interval interval="1000">A one second blink</my-blink-interval>
```

---

# All kinds of Blinks!!!

We have <my-blink-interval interval="1500">slow blinks</my-blink-interval> and <my-blink-interval interval="300">fast blinks</my-blink-interval>!

---

class: big, inverse, center, middle

# And <my-blink-interval interval="100">Seizure Blinks!!!!</my-blink-interval>

---

class: center, middle, big

# Now for something useful

???

- Blinks are fun and all, but what about something useful?
- What kind of component might we want to use, and reuse, on a web page?
- And how could these custom elements communicate and coordinate with each other?

---

class: center, middle, big

# User Management Components

???

- I have a set of Vue components that can work together to be a user management application.
- Three different components,
- a view component that shows users in a table,
- a component that lets you create a new user,
- a component that will filter shown users
- Like all good components, none of them require another to be available, so I can mix and match as I see fit.

---

# UserTable

``` html
<template>
     <table class="table">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Title</td>
                <td>Email</td>
                <td>Enabled</td>
            </tr>
        </thead>
        <tbody>
            <tr v-for="user in users" v-bind:key="user.id">
                <td>{{ user.id }}</td>
                <td>{{ user.firstName }} {{ user.lastName }}</td>
                <td>{{ user.title }}</td>
                <td>{{ user.email }}</td>
                <td>{{ user.enabled ? 'Yes' : 'No' }}</td>
            </tr>
        </tbody>
    </table>
</template>
```

???

- User table is pretty simple.
- Show users in a table!
- Where to the users come from?

---

# User Table

``` html
<script>
import userStore from '@/stores/user';
export default {
    name: 'user-table',
    computed: {
        users() {
*           return userStore.getters.users;
        }
    }
}
</script>
```

???

- From a Vuex store!
- The Vuex store is defined in a separate file and since this isn't a Vuex tutorial, I'm not going to get that much into it,
- It's just a normal Vuex store that you would use in any other Vue project.
- The Vuex store acts as our central data repository for the components.

---

# New User

``` html
<script>
import userStore from '@/stores/user';

export default {
    name: 'new-user',
...
    methods: {
        enableForm() {
            this.enabled = true;
        },
        performSubmit() {
            this.user.id = Math.floor(Math.random() * 1000);
*           userStore.commit('addUser', this.user);
            this.closeForm();
        },
...
    }
}
</script>
```

???

- The new user component has a form and commits data to the Vuex repository.

---

# User Filter

``` html
<script>
import userStore from '@/stores/user';
export default {

    name: 'user-filter',
    data() {
        return {
            filter: ''
        }
    },
    methods: {
        filterChange() {
*           userStore.commit('changeFilter', this.filter);
        }
    }
}
</script>
```

???

- The User filter sets the filter on the Vuex container.

---

# User Store

``` javascript
export default new Vuex.Store({
...
  getters: {
    users(state) {
*     return state.allUsers.filter((user) => {
        return user.firstName.includes(state.filterText) ||
          user.lastName.includes(state.filterText);
      });
    }
  },
  mutations: {
    addUser(state, user) {
*     state.allUsers.push(user);
    },
    changeFilter(state, newFilter) {
*     state.filterText = newFilter;
    },
    clear(state) {
      state.allUsers = [];
      state.filterText = '';
    }
  }
});
```

???

- And the Vuex store handles all the data changes.
- Getting the users will use the filter from the getters
- the two mutations handle adding a new user and changing the filter text.

---

# Demo

<demo-user-filter></demo-user-filter>
<demo-user-table></demo-user-table>
<demo-new-user></demo-new-user>

???

- USE TEST!
- I can now use these components on my page.
- And I can place them anywhere.
- One could be in a sidebar and one in the content, or one in the header and one in the footer.
- It doesn't matter, the browser just sees them as HTML elements.
- They just happen to be able to talk to each other by connecting to the same Vuex data store.
- They are pretty completely decoupled and, if I set up my props to pass in configuration information via attributes, extremely flexible.

---

# Building multiple components

???

- We're now building multiple components from one project.
- How do we build these components most effectively?

--

``` json
"build:wc":
  "vue-cli-service build --target wc-async 'src/components/*.vue'",
```

???

- We can use the CLI build script again to build multiple components.
- If we set the target to just `wc`, then the components would all be put in one file.
- You can load the components asynchronously using `wc-async`.
- This will create a loader JavaScript file in your `dist` folder and a separate file per component.
- Files are only loaded if needed by the current page.

---

# Building multiple components

``` json
"build:wc":
  "vue-cli-service build --target wc-async --name demo 'src/components/*.vue'",
```

???

- We add name.
- Here the name will be a prefix for all of your tags.
- By setting this to `demo`, my tags will be `demo-new-user`, etc. Like so.

---

# Async JavaScript files

![Async JavaScript files after build](img/async-file-list.png)

---

# Tag names

```HTML
<script src="js/demo.min.js"></script>

<demo-user-filter></demo-user-filter>
<demo-user-table></demo-user-table>
<demo-new-user></demo-new-user>
```

---

class: center, middle, big

# Who can have Vue components?

---

class: center, middle, big, inverse

# Everyone has Vue components!

---

class: center, middle

# Thank you

https://jerickson.net/js-custom-elements/